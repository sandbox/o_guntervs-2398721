INTRODUCTION
------------
The Block HTTP Referer module is made to easily block spambots from your site.
You can do this by entering the url or part of the url on the configuration page. If
$_SERVER['HTTP_REFERER'] matches with any of these urls, access will be blocked.


REQUIREMENTS
------------
No special requirements


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Add blocked referers on Administration » Configuration » Search and metadata » Block HTTP Referer

 MAINTAINERS
 -----------
 Current maintainers:
  * Gunter Van Steen (guntervs) - https://www.drupal.org/u/guntervs