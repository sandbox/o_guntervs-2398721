<?php

/**
 * Builds the Block HTTP Referer settings form.
 *
 * @param $form
 * @param $form_state
 */
function block_http_referer_admin_form($form, &$form_state) {
  $form['block_http_referer_list'] = array(
    '#title' => t('Referer list'),
    '#description' => t('Enter each referer on a new line. The check is done with a simple string matching.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('block_http_referer_list', ''),
  );

  return system_settings_form($form);
}